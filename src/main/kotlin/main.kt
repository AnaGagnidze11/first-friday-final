fun main() {

    val firstArray = arrayOf(3, 5, 6, 7, 7, 8, 3)
    println(countDifferentNumbers(firstArray))

    val secondArray = arrayOf(5, 3, 7, 2, 1)
    println(intersectCollection(firstArray, secondArray))

    println(unifyCollections(firstArray, secondArray))

    println(subCollection(firstArray))

    val myList = mutableListOf(3, 5, 6, 7, 7, 8, 3)
    println(secondMaxAndMin(myList))

}

// This function counts all distinct numbers in an array. I decided to just convert array to set, as set only has
// distinct values in it. And then return set's size.
fun countDifferentNumbers(array: Array<Int>): Int {
    val mySet = array.toSet()
    return mySet.size
}

// This function returns the intersection(Same numbers in two arrays). I used intersect function, which needs to have
// set as its argument, so i converted second array into a set.
fun intersectCollection(first: Array<Int>, second: Array<Int>): Set<Int> {
    val newSet = first.intersect(second.toSet())
    return newSet
}

// This function unifies two arrays. I can do that by adding second array to first, to do so I need first function
// to be mutable. This is why i converted first array into Mutable List. Then used addAll() function.
fun unifyCollections(first: Array<Int>, second: Array<Int>): MutableList<Int> {
    val newFirst = first.toMutableList()
    newFirst.addAll(second)
    return newFirst
}

// This function finds numbers which are less than average of all numbers in an array, and returns them.
// I wanted to be able to add those numbers to a new list, so I created a mutable List. Then i find those items, and
// simply add them to my mutable list by add() function.
fun subCollection(array: Array<Int>): MutableList<Int> {
    val average = array.average()
    val myMutableList = mutableListOf(0)
    myMutableList.clear()
    for (item in array) {
        if (item < average) {
            myMutableList.add(item)
        }
    }
    return myMutableList
}

// This function finds the second max and min numbers in an List. I chose the collection to be Mutable List,
// Because I wanted to be able to remove unwanted items(Like first max and min values) from the list.
// Firstly, I find first max and min values, remove them from the list and lastly, find second max and min.
fun secondMaxAndMin(list: MutableList<Int>): String {
    val firstMax = list.maxOrNull()
    val firstMin = list.minOrNull()

    list.remove(firstMax)
    list.remove(firstMin)

    val secondMax = list.maxOrNull()
    val secondMin = list.minOrNull()

    return "second Max number - $secondMax, Second Min Number - $secondMin"
}
